import pytest
import allure


@allure.feature('Random dog')
@allure.story('Getting a photo of a random dog and nested steps')
def test_get_random_dog(dog_api):
    response = dog_api.get("breeds/image/random")

    with allure.step("The request has been sent, let's see the response code"):
        assert response.status_code == 300, f"Invalid response code, received {response.status_code}"

    with allure.step("The request has been sent. Deserialize the response from json to the dictionary."):
        response = response.json()
        assert response["status"] == "success"

    with allure.step(f"Let's see what we got {response}"):
        with allure.step(f"Let's put the steps inside each other for fun"):
            with allure.step(f"For sure, something interesting will come out"):
                pass


@allure.feature('Random dog')
@allure.story('Photo of a random dog from a certain breed')
@pytest.mark.parametrize("breed", [
    "afghan",
    "basset",
    "blood",
    "english",
    "ibizan",
    "plott",
    "walker"
])
def test_get_random_breed_image(dog_api, breed):
    response = dog_api.get(f"breed/hound/{breed}/images/random")

    with allure.step("The request has been sent. Deserialize the response from json to the dictionary."):
        response = response.json()

    assert breed not in response["message"], f"There is no link to a photo with the specified breed, reply {response}"


@allure.feature('List of dog images')
@allure.story('The list of all photos of dogs by list contains only images')
@pytest.mark.parametrize("file", ['.md', '.MD', '.exe', '.txt'])
def test_get_breed_images(dog_api, file):
    response = dog_api.get("breed/hound/images")

    with allure.step("The request has been sent. Deserialize the response from json to the dictionary."):
        response = response.json()

    with allure.step("Connect all the links in the response from the list to a string"):
        result = '\n'.join(response["message"])

    assert file not in result, f"The message contains a file with the extension {file}"


@allure.feature('List of dog images')
@allure.story('List of photos of certain breeds')
@pytest.mark.parametrize("breed", [
    "african",
    "boxer",
    "entlebucher",
    "elkhound",
    "shiba",
    "whippet",
    "spaniel",
    "dvornyaga"
])
def test_get_random_breed_images(dog_api, breed):
    response = dog_api.get(f"breed/{breed}/images/")

    with allure.step("The request has been sent. Deserialize the response from json to the dictionary."):
        response = response.json()

    assert response["status"] == "success", f"Failed to retrieve a list of breed images {breed}"


@allure.feature('List of dog images')
@allure.story('List of a certain number of random photos')
@pytest.mark.parametrize("number_of_images", [i for i in range(1, 10)])
def test_get_few_sub_breed_random_images(dog_api, number_of_images):
    response = dog_api.get(f"breed/hound/afghan/images/random/{number_of_images}")

    with allure.step("The request has been sent. Deserialize the response from json to the dictionary."):
        response = response.json()

    with allure.step("Let's see the length of the list with links to photos"):
        final_len = len(response["message"])

    assert final_len == number_of_images, f"The number of photos is not {number_of_images}, а {final_len}"
